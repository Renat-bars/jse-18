package ru.tsc.almukhametov.tm.exception.system;

import ru.tsc.almukhametov.tm.exception.AbstractException;

public class UnknownStatusException extends AbstractException {

    public UnknownStatusException(final String status) {
        super("Error! Sort ``"+status+"`` was not found");
    }

}
