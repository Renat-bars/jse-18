package ru.tsc.almukhametov.tm.exception.system;

import ru.tsc.almukhametov.tm.exception.AbstractException;

public class UnknownRoleException extends AbstractException {

    public UnknownRoleException(final String role) {
        super("Error! Sort ``"+role+"`` was not found");
    }

}
