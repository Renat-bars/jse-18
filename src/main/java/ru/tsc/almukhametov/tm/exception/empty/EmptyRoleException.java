package ru.tsc.almukhametov.tm.exception.empty;

import ru.tsc.almukhametov.tm.exception.AbstractException;

public class EmptyRoleException extends AbstractException {

    public EmptyRoleException(){
        super("Error! Role is empty");
    }

}
