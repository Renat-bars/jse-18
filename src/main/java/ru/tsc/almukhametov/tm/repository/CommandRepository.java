package ru.tsc.almukhametov.tm.repository;

import ru.tsc.almukhametov.tm.api.repository.ICommandRepository;
import ru.tsc.almukhametov.tm.command.AbstractCommand;
import ru.tsc.almukhametov.tm.constant.ArgumentConst;
import ru.tsc.almukhametov.tm.constant.TerminalConst;
import ru.tsc.almukhametov.tm.model.Command;

import java.util.*;

public class CommandRepository implements ICommandRepository {

    private final Map<String, AbstractCommand> arguments = new LinkedHashMap<>();

    private final Map<String, AbstractCommand> commands = new LinkedHashMap<>();

    @Override
    public Collection<AbstractCommand> getCommands() { return commands.values(); }

    @Override
    public Collection<AbstractCommand> getArguments() { return arguments.values(); }

    @Override
    public Collection<String> getCommandNames() {
        final List<String> result = new ArrayList<>();
        for (final AbstractCommand command : commands.values()) {
            final String name = command.name();
            if (name == null || name.isEmpty()) continue;
            result.add(name);
        }
        return result;
    }

    @Override
    public Collection<String> getCommandArg() {
        final List<String> result = new ArrayList<>();
        for (final AbstractCommand command : arguments.values()) {
            final String arg = command.arg();
            if (arg == null || arg.isEmpty()) continue;
            result.add(arg);
        }
        return result;
    }

    @Override
    public AbstractCommand getCommandByName(String name) { return commands.get(name); }

    @Override
    public AbstractCommand getCommandByArg(String arg) { return arguments.get(arg); }

    @Override
    public void add(AbstractCommand command) {
        final String name = command.name();
        final String arg = command.arg();
        if (name != null) commands.put(name, command);
        if (arg != null) arguments.put(arg, command);
    }
}
