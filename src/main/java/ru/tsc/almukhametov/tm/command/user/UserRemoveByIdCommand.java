package ru.tsc.almukhametov.tm.command.user;

import ru.tsc.almukhametov.tm.command.AbstractUserCommand;
import ru.tsc.almukhametov.tm.constant.SystemDescriptionConst;
import ru.tsc.almukhametov.tm.constant.TerminalConst;
import ru.tsc.almukhametov.tm.model.User;
import ru.tsc.almukhametov.tm.util.TerminalUtil;

public class UserRemoveByIdCommand extends AbstractUserCommand {

    @Override
    public String name() {
        return TerminalConst.USER_REMOVE_BY_ID;
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return SystemDescriptionConst.USER_REMOVE_BY_ID;
    }

    @Override
    public void execute() {
        System.out.println("[REMOVE USER]");
        System.out.println("ENTER ID:");
        final String id = TerminalUtil.nextLine();
        getUserService().removeUserById(id);
    }

}
