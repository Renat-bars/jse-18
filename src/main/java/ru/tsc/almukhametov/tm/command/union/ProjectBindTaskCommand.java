package ru.tsc.almukhametov.tm.command.union;

import ru.tsc.almukhametov.tm.command.AbstractProjectTaskCommand;
import ru.tsc.almukhametov.tm.constant.SystemDescriptionConst;
import ru.tsc.almukhametov.tm.constant.TerminalConst;
import ru.tsc.almukhametov.tm.exception.entity.ProjectNotFoundException;
import ru.tsc.almukhametov.tm.exception.entity.TaskNotFoundException;
import ru.tsc.almukhametov.tm.model.Task;
import ru.tsc.almukhametov.tm.util.TerminalUtil;

public class ProjectBindTaskCommand extends AbstractProjectTaskCommand {

    @Override
    public String name() {
        return TerminalConst.TASK_BIND_TO_PROJECT;
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return SystemDescriptionConst.TASK_BIND_TO_PROJECT;
    }

    @Override
    public void execute() {
        System.out.println("[Bind task to project]");
        System.out.println("Enter task id");
        final String taskId = TerminalUtil.nextLine();
        if (taskId == null) throw new TaskNotFoundException();
        System.out.println("Enter project Id");
        final String projectId = TerminalUtil.nextLine();
        if (projectId == null) throw new ProjectNotFoundException();
        final Task task = getProjectTaskService().bindTaskById(projectId, taskId);
        if (task == null) throw new TaskNotFoundException();
    }
}
