package ru.tsc.almukhametov.tm.command.user;

import ru.tsc.almukhametov.tm.command.AbstractUserCommand;
import ru.tsc.almukhametov.tm.constant.SystemDescriptionConst;
import ru.tsc.almukhametov.tm.constant.TerminalConst;
import ru.tsc.almukhametov.tm.exception.system.AccessDeniedException;
import ru.tsc.almukhametov.tm.exception.system.UserNotFoundException;
import ru.tsc.almukhametov.tm.model.User;
import ru.tsc.almukhametov.tm.util.TerminalUtil;

public class UserChangePasswordCommand extends AbstractUserCommand {

    @Override
    public String name() {
        return TerminalConst.USER_CHANGE_PASSWORD;
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return SystemDescriptionConst.USER_CHANGE_PASSWORD;
    }

    @Override
    public void execute() {
        final String userId = getAuthenticationService().getUserId();
        System.out.println("CHANGE PASSWORD");
        System.out.println("ENTER NEW PASSWORD:");
        final String password = TerminalUtil.nextLine();
        getUserService().setPassword(userId, password);
        System.out.println("[OK]");
    }

}
