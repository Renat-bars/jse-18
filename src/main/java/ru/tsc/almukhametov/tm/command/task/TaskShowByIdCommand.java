package ru.tsc.almukhametov.tm.command.task;

import ru.tsc.almukhametov.tm.command.AbstractTaskCommand;
import ru.tsc.almukhametov.tm.constant.SystemDescriptionConst;
import ru.tsc.almukhametov.tm.constant.TerminalConst;
import ru.tsc.almukhametov.tm.exception.entity.TaskNotFoundException;
import ru.tsc.almukhametov.tm.model.Task;
import ru.tsc.almukhametov.tm.util.TerminalUtil;

public class TaskShowByIdCommand extends AbstractTaskCommand {

    @Override
    public String name() {
        return TerminalConst.TASK_SHOW_BY_ID;
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return SystemDescriptionConst.TASK_SHOW_BY_ID;
    }

    @Override
    public void execute() {
        System.out.println("Enter Id");
        final String id = TerminalUtil.nextLine();
        final Task task = getTaskService().findById(id);
        if (task == null) throw new TaskNotFoundException();
        showTasks(task);
    }
}
