package ru.tsc.almukhametov.tm.command.project;

import ru.tsc.almukhametov.tm.command.AbstractProjectCommand;
import ru.tsc.almukhametov.tm.constant.SystemDescriptionConst;
import ru.tsc.almukhametov.tm.constant.TerminalConst;
import ru.tsc.almukhametov.tm.enumerated.Sort;
import ru.tsc.almukhametov.tm.model.Project;
import ru.tsc.almukhametov.tm.util.TerminalUtil;

import java.util.Arrays;
import java.util.List;

public class ProjectShowListCommand extends AbstractProjectCommand {

    @Override
    public String name() {
        return TerminalConst.PROJECT_LIST;
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return SystemDescriptionConst.PROJECT_LIST;
    }

    @Override
    public void execute() {
        System.out.println("Enter sort");
        System.out.println(Arrays.toString(Sort.values()));
        final String sort = TerminalUtil.nextLine();

        List<Project> projects;
        if (sort == null || sort.isEmpty()) projects = IServiceLocator.getProjectService().findAll();
        else {
            Sort sortType = Sort.valueOf(sort);
            System.out.println(sortType.getDisplayName());
            projects = IServiceLocator.getProjectService().findAll(sortType.getComparator());
        }
        int index = 1;
        for (final Project project : projects) {
            System.out.println(index + ". " + project.toString());
            index++;
        }
    }
}
