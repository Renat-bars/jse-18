package ru.tsc.almukhametov.tm.command.system;

import ru.tsc.almukhametov.tm.command.AbstractCommand;
import ru.tsc.almukhametov.tm.constant.ArgumentConst;
import ru.tsc.almukhametov.tm.constant.SystemDescriptionConst;
import ru.tsc.almukhametov.tm.constant.TerminalConst;

import java.util.Collection;

public class Commands extends AbstractCommand {

    @Override
    public String name() {
        return TerminalConst.COMMANDS;
    }

    @Override
    public String arg() {
        return ArgumentConst.COMMANDS;
    }

    @Override
    public String description() {
        return SystemDescriptionConst.COMMANDS;
    }

    @Override
    public void execute() {
        final Collection<AbstractCommand> commands = IServiceLocator.getCommandService().getCommands();
        for (final AbstractCommand command : commands) { System.out.println(command.name()); }
    }
}
