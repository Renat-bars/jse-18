package ru.tsc.almukhametov.tm.command.task;

import ru.tsc.almukhametov.tm.command.AbstractTaskCommand;
import ru.tsc.almukhametov.tm.constant.SystemDescriptionConst;
import ru.tsc.almukhametov.tm.constant.TerminalConst;
import ru.tsc.almukhametov.tm.exception.empty.EmptyIdException;
import ru.tsc.almukhametov.tm.exception.entity.TaskNotFoundException;
import ru.tsc.almukhametov.tm.model.Task;
import ru.tsc.almukhametov.tm.util.TerminalUtil;

public class TaskUpdateByIdCommand extends AbstractTaskCommand {
    @Override
    public String name() {
        return TerminalConst.TASK_UPDATE_BY_ID;
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return SystemDescriptionConst.TASK_UPDATE_BY_ID;
    }

    @Override
    public void execute() {
        System.out.println("Enter Id");
        final String id = TerminalUtil.nextLine();
        if (!getTaskService().existById(id)) throw new EmptyIdException();
        System.out.println("Enter Name");
        final String name = TerminalUtil.nextLine();
        System.out.println("Enter description");
        final String description = TerminalUtil.nextLine();
        final Task taskUpdated = getTaskService().updateById(id, name, description);
        if (taskUpdated == null) throw new TaskNotFoundException();
    }
}
