package ru.tsc.almukhametov.tm.command.system;

import ru.tsc.almukhametov.tm.command.AbstractCommand;
import ru.tsc.almukhametov.tm.constant.ArgumentConst;
import ru.tsc.almukhametov.tm.constant.SystemDescriptionConst;
import ru.tsc.almukhametov.tm.constant.TerminalConst;

import java.util.Collection;

public class ArgumentsShowCommand extends AbstractCommand {

    @Override
    public String name() {
        return TerminalConst.ARGUMENTS;
    }

    @Override
    public String arg() {
        return ArgumentConst.ARGUMENTS;
    }

    @Override
    public String description() {
        return SystemDescriptionConst.ARGUMENTS;
    }

    @Override
    public void execute() {
        System.out.println("[ARGUMENTS]");
        final Collection<AbstractCommand> arguments = IServiceLocator.getCommandService().getArguments();
        for (final AbstractCommand arg : arguments) { System.out.println(arg.name());}
    }
}
