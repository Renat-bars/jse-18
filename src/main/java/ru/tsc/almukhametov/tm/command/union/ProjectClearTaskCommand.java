package ru.tsc.almukhametov.tm.command.union;

import ru.tsc.almukhametov.tm.command.AbstractProjectTaskCommand;
import ru.tsc.almukhametov.tm.constant.SystemDescriptionConst;
import ru.tsc.almukhametov.tm.constant.TerminalConst;

public class ProjectClearTaskCommand extends AbstractProjectTaskCommand {

    @Override
    public String name() {
        return TerminalConst.PROJECT_CLEAR;
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return SystemDescriptionConst.PROJECT_CLEAR;
    }

    @Override
    public void execute() {
        System.out.println("[ClEAR PROJECTS]");
        getProjectTaskService().clearProjects();
        System.out.println("[SUCCESS CLEAR]");
    }
}
