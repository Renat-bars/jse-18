package ru.tsc.almukhametov.tm.command.task;

import ru.tsc.almukhametov.tm.command.AbstractTaskCommand;
import ru.tsc.almukhametov.tm.constant.SystemDescriptionConst;
import ru.tsc.almukhametov.tm.constant.TerminalConst;
import ru.tsc.almukhametov.tm.exception.empty.EmptyIndexException;
import ru.tsc.almukhametov.tm.exception.entity.TaskNotFoundException;
import ru.tsc.almukhametov.tm.model.Task;
import ru.tsc.almukhametov.tm.util.TerminalUtil;

public class TaskUpdateByIndexCommand extends AbstractTaskCommand {
    @Override
    public String name() {
        return TerminalConst.TASK_UPDATE_BY_INDEX;
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return SystemDescriptionConst.TASK_UPDATE_BY_INDEX;
    }

    @Override
    public void execute() {
        System.out.println("Enter Index");
        final Integer index = TerminalUtil.nextNumber() - 1;
        if (!getTaskService().existByIndex(index)) throw new EmptyIndexException();
        System.out.println("Enter Name");
        final String name = TerminalUtil.nextLine();
        System.out.println("Enter description");
        final String description = TerminalUtil.nextLine();
        final Task taskUpdate = getTaskService().updateByIndex(index, name, description);
        if (taskUpdate == null) throw new TaskNotFoundException();
    }
}
