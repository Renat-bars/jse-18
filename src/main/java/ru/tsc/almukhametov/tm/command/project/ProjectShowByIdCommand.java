package ru.tsc.almukhametov.tm.command.project;

import ru.tsc.almukhametov.tm.command.AbstractProjectCommand;
import ru.tsc.almukhametov.tm.constant.SystemDescriptionConst;
import ru.tsc.almukhametov.tm.constant.TerminalConst;
import ru.tsc.almukhametov.tm.exception.entity.ProjectNotFoundException;
import ru.tsc.almukhametov.tm.model.Project;
import ru.tsc.almukhametov.tm.util.TerminalUtil;

public class ProjectShowByIdCommand extends AbstractProjectCommand {

    @Override
    public String name() {
        return TerminalConst.PROJECT_SHOW_BY_ID;
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return SystemDescriptionConst.PROJECT_SHOW_BY_ID;
    }

    @Override
    public void execute() {
        System.out.println("Enter Id");
        final String id = TerminalUtil.nextLine();
        final Project project = getProjectService().findById(id);
        if (project == null) throw new ProjectNotFoundException();
        showProject(project);
    }
}
