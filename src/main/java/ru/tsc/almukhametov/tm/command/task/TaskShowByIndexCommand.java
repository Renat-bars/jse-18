package ru.tsc.almukhametov.tm.command.task;

import ru.tsc.almukhametov.tm.command.AbstractTaskCommand;
import ru.tsc.almukhametov.tm.constant.SystemDescriptionConst;
import ru.tsc.almukhametov.tm.constant.TerminalConst;
import ru.tsc.almukhametov.tm.exception.entity.TaskNotFoundException;
import ru.tsc.almukhametov.tm.model.Task;
import ru.tsc.almukhametov.tm.util.TerminalUtil;

public class TaskShowByIndexCommand extends AbstractTaskCommand {

    @Override
    public String name() {
        return TerminalConst.TASK_SHOW_BY_INDEX;
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return SystemDescriptionConst.TASK_SHOW_BY_INDEX;
    }

    @Override
    public void execute() {
        System.out.println("Enter Index");
        final Integer index = TerminalUtil.nextNumber() - 1;
        final Task task = getTaskService().findByIndex(index);
        if (task == null) throw new TaskNotFoundException();
        showTasks(task);
    }
}
