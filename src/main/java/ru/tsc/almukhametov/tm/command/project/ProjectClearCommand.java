package ru.tsc.almukhametov.tm.command.project;

import ru.tsc.almukhametov.tm.command.AbstractProjectCommand;
import ru.tsc.almukhametov.tm.constant.SystemDescriptionConst;
import ru.tsc.almukhametov.tm.constant.TerminalConst;

public class ProjectClearCommand extends AbstractProjectCommand {

    @Override
    public String name() {
        return TerminalConst.PROJECT_CLEAR;
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return SystemDescriptionConst.PROJECT_CLEAR;
    }

    @Override
    public void execute() {
        System.out.println("[ClEAR PROJECTS]");
        getProjectService().clear();
        System.out.println("[SUCCESS CLEAR]");
    }
}
