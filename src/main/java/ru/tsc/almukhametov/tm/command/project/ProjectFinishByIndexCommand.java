package ru.tsc.almukhametov.tm.command.project;

import ru.tsc.almukhametov.tm.command.AbstractProjectCommand;
import ru.tsc.almukhametov.tm.constant.SystemDescriptionConst;
import ru.tsc.almukhametov.tm.constant.TerminalConst;
import ru.tsc.almukhametov.tm.exception.entity.ProjectNotFoundException;
import ru.tsc.almukhametov.tm.model.Project;
import ru.tsc.almukhametov.tm.util.TerminalUtil;

import java.util.Date;

public class ProjectFinishByIndexCommand extends AbstractProjectCommand {

    @Override
    public String name() {
        return TerminalConst.PROJECT_FINISH_BY_INDEX;
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return SystemDescriptionConst.PROJECT_FINISH_BY_INDEX;
    }

    @Override
    public void execute() {
        System.out.println("Enter Index");
        final Integer index = TerminalUtil.nextNumber() - 1;
        final Project project = getProjectService().finishByIndex(index);
        if (project == null) throw new ProjectNotFoundException();
        else project.setFinishDate(new Date());
    }
}
