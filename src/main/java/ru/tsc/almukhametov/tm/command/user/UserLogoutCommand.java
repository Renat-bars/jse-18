package ru.tsc.almukhametov.tm.command.user;

import ru.tsc.almukhametov.tm.command.AbstractUserCommand;
import ru.tsc.almukhametov.tm.constant.SystemDescriptionConst;
import ru.tsc.almukhametov.tm.constant.TerminalConst;
import ru.tsc.almukhametov.tm.util.TerminalUtil;

public class UserLogoutCommand extends AbstractUserCommand {

    @Override
    public String name() {
        return TerminalConst.LOGOUT;
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return SystemDescriptionConst.LOGOUT;
    }

    @Override
    public void execute() {
        System.out.println("[LOGOUT]");
        getAuthenticationService().logout();
    }

}
