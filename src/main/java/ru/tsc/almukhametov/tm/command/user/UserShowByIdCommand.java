package ru.tsc.almukhametov.tm.command.user;

import ru.tsc.almukhametov.tm.command.AbstractUserCommand;
import ru.tsc.almukhametov.tm.constant.SystemDescriptionConst;
import ru.tsc.almukhametov.tm.constant.TerminalConst;
import ru.tsc.almukhametov.tm.util.TerminalUtil;

public class UserShowByIdCommand extends AbstractUserCommand {
    @Override
    public String name() {
        return TerminalConst.USER_SHOW_BY_ID;
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return SystemDescriptionConst.USER_SHOW_BY_ID;
    }

    @Override
    public void execute() {
        System.out.println("[SHOW USER]");
        System.out.println("ENTER ID:");
        final String id = TerminalUtil.nextLine();
        getUserService().findById(id);
    }
}
