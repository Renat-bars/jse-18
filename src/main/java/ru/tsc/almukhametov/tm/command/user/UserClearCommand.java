package ru.tsc.almukhametov.tm.command.user;

import ru.tsc.almukhametov.tm.command.AbstractUserCommand;
import ru.tsc.almukhametov.tm.constant.SystemDescriptionConst;
import ru.tsc.almukhametov.tm.constant.TerminalConst;

public class UserClearCommand extends AbstractUserCommand {

    @Override
    public String name() {
        return TerminalConst.USER_CLEAR;
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return SystemDescriptionConst.USER_CLEAR;
    }

    @Override
    public void execute() {
        System.out.println("[ClEAR USERS]");
        getUserService().clear();
        System.out.println("[SUCCESS CLEAR]");
    }

}
