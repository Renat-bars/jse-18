package ru.tsc.almukhametov.tm.command.user;

import ru.tsc.almukhametov.tm.command.AbstractUserCommand;
import ru.tsc.almukhametov.tm.constant.SystemDescriptionConst;
import ru.tsc.almukhametov.tm.constant.TerminalConst;
import ru.tsc.almukhametov.tm.model.User;

import java.util.List;

public class UserListShowCommand extends AbstractUserCommand {

    @Override
    public String name() {
        return TerminalConst.USER_LIST;
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return SystemDescriptionConst.USER_LIST;
    }

    @Override
    public void execute() {
        System.out.println("[USER LIST]");
        final List<User> users = getUserService().findAll();
        int index = 1;
        for (final User user : users) {
            System.out.println(index + ". " + user);
            index++;
        }
    }

}
