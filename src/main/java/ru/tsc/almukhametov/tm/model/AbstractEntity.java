package ru.tsc.almukhametov.tm.model;

import java.util.UUID;

public abstract class AbstractEntity {

    protected final String id = UUID.randomUUID().toString();

    public String getId() { return id; }

}
