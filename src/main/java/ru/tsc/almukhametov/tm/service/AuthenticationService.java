package ru.tsc.almukhametov.tm.service;

import ru.tsc.almukhametov.tm.api.service.IAuthenticationService;
import ru.tsc.almukhametov.tm.api.service.IUserService;
import ru.tsc.almukhametov.tm.exception.empty.EmptyLoginException;
import ru.tsc.almukhametov.tm.exception.empty.EmptyPasswordException;
import ru.tsc.almukhametov.tm.exception.system.AccessDeniedException;
import ru.tsc.almukhametov.tm.model.User;
import ru.tsc.almukhametov.tm.util.HashUtil;

public class AuthenticationService implements IAuthenticationService {

    private final IUserService userService;

    private String userId;

    public AuthenticationService (final IUserService userService) { this.userService = userService; }

    @Override
    public User getUser() {
        final String userId = getUserId();
        return userService.findById(userId);
    }

    @Override
    public String getUserId() {
        if (userId == null ) throw new AccessDeniedException();
        return userId;
    }

    @Override
    public boolean isAuthentication() {
        return userId == null;
    }

    @Override
    public void logout() { userId = null; }

    @Override
    public void login(final String login, final String password) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        if (password == null || password.isEmpty()) throw new EmptyPasswordException();
        final User user = userService.findByLogin(login);
        if (user == null) throw new AccessDeniedException();
        final String hash = HashUtil.salt(password);
        if (hash == null) throw new AccessDeniedException();
        if (!hash.equals(user.getPasswordHash())) throw new AccessDeniedException();
        userId = user.getId();
    }

    @Override
    public void registry(final String login, final String password, final String email) {
        userService.create(login, password, email);
    }

}
