package ru.tsc.almukhametov.tm.service;

import ru.tsc.almukhametov.tm.api.repository.IProjectRepository;
import ru.tsc.almukhametov.tm.api.repository.ITaskRepository;
import ru.tsc.almukhametov.tm.api.service.IProjectTaskService;
import ru.tsc.almukhametov.tm.exception.empty.EmptyIdException;
import ru.tsc.almukhametov.tm.exception.empty.EmptyIndexException;
import ru.tsc.almukhametov.tm.exception.empty.EmptyNameException;
import ru.tsc.almukhametov.tm.exception.entity.ProjectNotFoundException;
import ru.tsc.almukhametov.tm.model.Project;
import ru.tsc.almukhametov.tm.model.Task;

import java.util.List;

public class ProjectTaskService implements IProjectTaskService {

    private IProjectRepository projectRepository;

    private ITaskRepository taskRepository;

    public ProjectTaskService(IProjectRepository projectRepository, ITaskRepository taskRepository) {
        this.projectRepository = projectRepository;
        this.taskRepository = taskRepository;
    }

    @Override
    public List<Task> findAllTaskByProjectId(final String projectId) {
        if (projectId == null || projectId.isEmpty()) throw new EmptyIdException();
        return taskRepository.findAllTaskByProjectId(projectId);
    }

    @Override
    public Task bindTaskById(final String projectId, final String taskId) {
        if (projectId == null || projectId.isEmpty()) throw new EmptyIdException();
        if (taskId == null || taskId.isEmpty()) throw new EmptyIdException();
        if (!projectRepository.existById(projectId)) return null;
        if (!taskRepository.existById(taskId)) return null;
        return taskRepository.bindTaskById(projectId, taskId);
    }

    @Override
    public Task unbindTaskById(final String projectId, final String taskId) {
        if (projectId == null || projectId.isEmpty()) throw new EmptyIdException();
        if (taskId == null || taskId.isEmpty()) throw new EmptyIdException();
        if (!projectRepository.existById(projectId)) return null;
        if (!taskRepository.existById(taskId)) return null;
        return taskRepository.unbindTaskById(projectId, taskId);
    }

    public Project removeById(final String projectId) {
        if (projectId == null || projectId.isEmpty()) throw new EmptyIdException();
        taskRepository.removeAllTaskByProjectId(projectId);
        return projectRepository.removeById(projectId);
    }

    public Project removeByIndex(final Integer index) {
        if (index == null || index < 0) throw new EmptyIndexException();
        if (projectRepository.getSize() < index - 1) throw new ProjectNotFoundException();
        final String projectId = projectRepository.findByIndex(index).getId();
        taskRepository.removeAllTaskByProjectId(projectId);
        return projectRepository.removeByIndex(index);
    }

    public Project removeByName(final String name) {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        final String projectId = projectRepository.findByName(name).getId();
        taskRepository.removeAllTaskByProjectId(projectId);
        return projectRepository.removeByName(name);
    }

    public void clearProjects() {
        for (Project project : projectRepository.findAll())
            taskRepository.removeAllTaskByProjectId(project.getId());
        projectRepository.clear();
    }

}
