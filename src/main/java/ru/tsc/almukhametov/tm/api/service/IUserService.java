package ru.tsc.almukhametov.tm.api.service;

import ru.tsc.almukhametov.tm.enumerated.Role;
import ru.tsc.almukhametov.tm.model.User;

import java.util.List;

public interface IUserService {

    List<User> findAll();

    User create(String login, String password);

    User create(String login, String password, String email);

    User create(String login, String password, Role role);

    User findById(String id);

    User findByLogin(String login);

    boolean isLoginExists(String login);

    User findByEmail(String email);

    boolean isEmailExists(String email);

    User removeUser(User user);

    User removeUserById(String id);

    User removeUserByLogin(String login);

    User updateUser(String userId, String firstName, String lastName, String middleName);

    User setPassword(String userId, String password);

    User setRole(String userId, Role role);

    void clear();
}
