package ru.tsc.almukhametov.tm.api.service;

import ru.tsc.almukhametov.tm.model.User;

public interface IAuthenticationService {

    User getUser();

    String getUserId();

    boolean isAuthentication();

    void logout();

    void login(String login, String password);

    void registry(String login, String password, String email);

}
