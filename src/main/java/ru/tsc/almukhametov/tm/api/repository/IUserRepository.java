package ru.tsc.almukhametov.tm.api.repository;

import ru.tsc.almukhametov.tm.enumerated.Role;
import ru.tsc.almukhametov.tm.model.User;

import java.util.List;

public interface IUserRepository {

    List<User> findAll();

    User add(User user);

    void clear();

    User findById(String id);

    User findByLogin(String login);

    boolean isLoginExists(String login);

    User findByEmail(String email);

    boolean isEmailExists(String email);

    User removeUser(User user);

    User removeUserById(String id);

    User removeUserByLogin(String login);

    User updateUser(String userId, String firstName, String lastName, String middleName);

    User setPassword(String userId, String password);

}
