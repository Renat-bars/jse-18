package ru.tsc.almukhametov.tm.component;

import ru.tsc.almukhametov.tm.api.repository.ICommandRepository;
import ru.tsc.almukhametov.tm.api.repository.IProjectRepository;
import ru.tsc.almukhametov.tm.api.repository.ITaskRepository;
import ru.tsc.almukhametov.tm.api.repository.IUserRepository;
import ru.tsc.almukhametov.tm.api.service.*;
import ru.tsc.almukhametov.tm.command.AbstractCommand;
import ru.tsc.almukhametov.tm.command.project.*;

import ru.tsc.almukhametov.tm.command.system.*;
import ru.tsc.almukhametov.tm.command.task.*;
import ru.tsc.almukhametov.tm.command.union.*;
import ru.tsc.almukhametov.tm.constant.TerminalConst;

import ru.tsc.almukhametov.tm.enumerated.Role;
import ru.tsc.almukhametov.tm.enumerated.Status;
import ru.tsc.almukhametov.tm.exception.system.UnknownArgumentException;
import ru.tsc.almukhametov.tm.exception.system.UnknownCommandException;
import ru.tsc.almukhametov.tm.model.Project;
import ru.tsc.almukhametov.tm.model.Task;
import ru.tsc.almukhametov.tm.repository.CommandRepository;
import ru.tsc.almukhametov.tm.repository.ProjectRepository;
import ru.tsc.almukhametov.tm.repository.TaskRepository;
import ru.tsc.almukhametov.tm.repository.UserRepository;
import ru.tsc.almukhametov.tm.service.*;
import ru.tsc.almukhametov.tm.util.TerminalUtil;

import static ru.tsc.almukhametov.tm.enumerated.Status.*;

public class Bootstrap implements IServiceLocator {

    private final ICommandRepository commandRepository = new CommandRepository();

    private final ICommandService commandService = new CommandService(commandRepository);

    private final IProjectRepository projectRepository = new ProjectRepository();

    private final IProjectService projectService = new ProjectService(projectRepository);

    private final ITaskRepository taskRepository = new TaskRepository();

    private final TaskService taskService = new TaskService(taskRepository);

    private final IProjectTaskService projectTaskService = new ProjectTaskService(projectRepository, taskRepository);

    private final ILogService logService = new LogService();

    private final IUserRepository userRepository = new UserRepository();

    private final IUserService userService = new UserService(userRepository);

    private final IAuthenticationService authenticationService = new AuthenticationService(userService);

    public void start(final String[] args) {
        System.out.println("** Welcome to THE REAL WORLD **");
        parseArgs(args);
        initData();
        initUsers();
        process();
    }

    private void initUsers() {
        userService.create("test", "test", "test@test.ru");
        userService.create("admin", "admin", Role.ADMIN);
    }

    private void initData() {
        projectService.add(new Project("Drum & Bass", "Music genre")).setStatus(COMPLETED);
        projectService.add(new Project("Lexus", "Vehicles factory")).setStatus(Status.NOT_STARTED);
        projectService.add(new Project("FAW", "Vehicles factory")).setStatus(Status.NOT_STARTED);
        projectService.add(new Project("ROSATOM", "Nuclear plant")).setStatus(Status.COMPLETED);
        projectService.add(new Project("Sport", "Citius, Altius, Fortius")).setStatus(Status.IN_PROGRESS);
        taskService.add(new Task("Neurophunk", "podcast")).setStatus(Status.COMPLETED);
        taskService.add(new Task("IS500", "Sedan cars")).setStatus(Status.NOT_STARTED);
        taskService.add(new Task("NPP", "Balaklavskaya")).setStatus(Status.COMPLETED);
        taskService.add(new Task("Hockey", "Tampa")).setStatus(Status.IN_PROGRESS);
    }

    private void registry(final AbstractCommand command) {
        if (command == null) return;
        command.setIServiceLocator(this);
        commandService.add(command);
    }

    public void parseArgs(String[] args) {
        if (args == null || args.length == 0) return;
        final String arg = args[0];
        parseArg(arg);
        System.exit(0);
    }

    public void parseArg(final String arg) {
        if (arg == null || arg.isEmpty()) return;
        final AbstractCommand command = commandService.getCommandByArg(arg);
        if (command == null) throw new UnknownArgumentException(arg);
        command.execute();
    }

    {
        registry(new Commands());
        registry(new AboutShowCommand());
        registry(new ArgumentsShowCommand());
        registry(new VersionShowCommand());
        registry(new InfoShowCommand());
        registry(new HelpCommand());
        registry(new ExitCommand());
        registry(new ProjectClearTaskCommand());
        registry(new ProjectCreateCommand());
        registry(new ProjectShowListCommand());
        registry(new ProjectRemoveByIdCommand());
        registry(new ProjectRemoveByIndexCommand());
        registry(new ProjectRemoveByNameCommand());
        registry(new ProjectShowByIdCommand());
        registry(new ProjectShowByIndexCommand());
        registry(new ProjectShowByNameCommand());
        registry(new ProjectStartByIdCommand());
        registry(new ProjectStartByIndexCommand());
        registry(new ProjectStartByNameCommand());
        registry(new ProjectUpdateByIdCommand());
        registry(new ProjectUpdateByIndexCommand());
        registry(new ProjectFinishByIdCommand());
        registry(new ProjectFinishByIndexCommand());
        registry(new ProjectFinishByNameCommand());
        registry(new ProjectChangeByIdCommand());
        registry(new ProjectChangeByIndexCommand());
        registry(new ProjectChangeByNameCommand());
        registry(new ProjectBindTaskCommand());
        registry(new ProjectUnbindTaskCommand());
        registry(new ProjectClearTaskCommand());
        registry(new ProjectFindAllTasksCommand());
        registry(new ProjectRemoveTasksByIdCommand());
        registry(new ProjectRemoveTasksByIndexCommand());
        registry(new ProjectRemoveTasksByNameCommand());
        registry(new TaskShowListCommand());
        registry(new TaskCreateCommand());
        registry(new TaskClearCommand());
        registry(new TaskShowByIdCommand());
        registry(new TaskShowByIndexCommand());
        registry(new TaskShowByNameCommand());
        registry(new TaskRemoveByIdCommand());
        registry(new TaskRemoveByIndexCommand());
        registry(new TaskRemoveByNameCommand());
        registry(new TaskUpdateByIdCommand());
        registry(new TaskUpdateByIndexCommand());
        registry(new TaskStartByIdCommand());
        registry(new TaskStartByIndexCommand());
        registry(new TaskStartByNameCommand());
        registry(new TaskFinishByIdCommand());
        registry(new TaskFinishByIndexCommand());
        registry(new TaskFinishByNameCommand());
        registry(new TaskChangeByIdCommand());
        registry(new TaskChangeByIndexCommand());
        registry(new TaskChangeByNameCommand());
    }

    public void parseCommands(final String command) {
        if (command == null || command.isEmpty()) return;
        AbstractCommand abstractCommand = commandService.getCommandByName(command);
        if (abstractCommand == null) throw new UnknownCommandException(command);
        abstractCommand.execute();
    }

    private void process() {
        logService.debug("Test environment.");
        String command = "";
        while (!TerminalConst.EXIT.equals(command)) {
            try {
                System.out.println("ENTER COMMAND:");
                command = TerminalUtil.nextLine();
                logService.command(command);
                parseCommands(command);
                logService.info("Completed");
            } catch (final Exception e) {
                logService.error(e);
            }
        }
    }

    @Override
    public ITaskService getTaskService() { return taskService; }

    @Override
    public IProjectService getProjectService() { return projectService; }

    @Override
    public IProjectTaskService getProjectTaskService() { return projectTaskService; }

    @Override
    public ICommandService getCommandService() { return commandService; }

    @Override
    public IAuthenticationService getAuthenticationService() { return authenticationService; }

    @Override
    public IUserService getUserService() { return userService; }

}
